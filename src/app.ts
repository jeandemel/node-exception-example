import Express from 'express';
import { doStuff } from './service';
import {MonException} from './exceptions'


const app = Express();

app.get('/api/test/:valeur', async (req:Express.Request,res:Express.Response) => {
    try {
        return res.json(doStuff(req.params.valeur));
    } catch (error) {
        if(error instanceof MonException) {
            return res.status(error.status).json({error: error.message});
        }
        return res.status(500).json({error: 'Server error'});
    }
})


app.listen(8888, () => console.log('listening on 8888'));