import { PasTrouver, PasBon } from "./exceptions";


export function doStuff(param:string) {
    if(param == 'blorp') {
        throw new PasTrouver();
    }
    if(param == 'blurp') {
        throw new PasBon();
    }

    return 'noice';
}