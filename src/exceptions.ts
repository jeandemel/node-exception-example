

export class MonException extends Error {
    status:number;
    constructor(message:string, status:number) {
        super(message);
        this.status = status;
    }
}


export class PasTrouver extends MonException {
    constructor() {
        super('Resource not found', 404);
    }
}

export class PasBon extends MonException {
    constructor() {
        super('Incorrect data received', 400);
    }
}